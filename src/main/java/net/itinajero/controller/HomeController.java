package net.itinajero.controller;

//import java.util.ArrayList;
import java.util.Date;
//import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.itinajero.model.Perfil;
import net.itinajero.model.Usuario;
import net.itinajero.model.Vacante;
import net.itinajero.service.ICategoriasService;
import net.itinajero.service.IUsuariosService;
import net.itinajero.service.IVacantesService;

@Controller
public class HomeController {
	
	@Autowired
	private IVacantesService serviceVacantes;
	
	@Autowired
	private IUsuariosService serviceUsuarios;
	
	@Autowired
	private ICategoriasService serviceCategorias;
	
	@Autowired 
	private PasswordEncoder passwordEncoder;
	
	/*
	@GetMapping("/tabla")
	public ModelAndView mostrarTabla() {
		ModelAndView model = new ModelAndView();
		List<Vacante> lista = serviceVacantes.buscarTodas();
		model.addObject("vacantes", lista);
		model.setViewName("tabla");
		return model;
	}
	
	@GetMapping("/detalle")
	public ModelAndView mostrarDetalle() {
		ModelAndView model = new ModelAndView();
		Vacante vacante = new Vacante();
		vacante.setNombre("Full Stack Java");
		vacante.setDescripcion("Se solicita Desarrollador Java");
		vacante.setFecha(new Date());
		vacante.setSalario(2000000.00);
		model.addObject("vacante", vacante);
		model.setViewName("detalle");
		return model;
	}
	
	@GetMapping("/listado")
	public ModelAndView mostrarListado() {
		ModelAndView model = new ModelAndView();
		List<String> lista = new ArrayList<>();
		lista.add("Javascript");
		lista.add("SpringMVC");
		lista.add("Big Data");
		lista.add("Full Stack");
		model.addObject("listado", lista);
		model.setViewName("listado");
		return model;
	}
	*/
	
	
	@GetMapping("/")
	public String mostrarHome(Model model) {
		return "home";
	}
	
	//Peticion desde menu.html (Registrarse)
	@GetMapping("/signup")
	public String registrarse(Usuario usuario) {
		return "formRegistro";
	}
	
	//Pëticion desde el formulario de registro
	@PostMapping("/signup")
	public String guardarRegistro(Usuario usuario, RedirectAttributes attributes) {
		usuario.setEstatus(1); // Activado por defecto
		usuario.setFechaRegistro(new Date()); // Fecha de Registro, la fecha actual del servidor
		
		// Creamos el Perfil que le asignaremos al usuario nuevo
		Perfil perfil = new Perfil();
		perfil.setId(3); // Perfil USUARIO POR DEFECTO
		usuario.agregar(perfil);
		
		//Encriptamos el password del usuario para ser guardada en la BD.
		String password = usuario.getPassword();
		usuario.setPassword(passwordEncoder.encode(password));
		
		
		/**
		 * Guardamos el usuario en la base de datos. El Perfil se guarda automaticamente
		 */
		serviceUsuarios.guardar(usuario);
				
		attributes.addFlashAttribute("msg", "El registro fue guardado correctamente!");
		
		return "redirect:/usuarios/index";
	}
	
	//Usamos ModelAttribute ya que estamos haciendo DataBinding con objeto el formulario.
	@GetMapping("/search")
	public String buscar(@ModelAttribute("search") Vacante vacante, Model model) {
		System.out.println("Buscando por: " + vacante);
		
		/* Example Matcher nos permite buscar por las propiedades no nulas de un objeto.
		 * Esto para la busqueda de vacantes por descripcion y sin categoria.
		 * Si no colocamos este matcher, la busqueda no arrojara resultados.
		 * Se debe buscar la descripcion de la vacante con un LIKE y no con un igual (=).
		 * En resumen la busqueda quedaria como: where descripcion like '%%'
		 * */
		ExampleMatcher matcher = ExampleMatcher.matching().withMatcher("descripcion", ExampleMatcher.GenericPropertyMatchers.contains());
				
		Example<Vacante> example = Example.of(vacante, matcher);
		model.addAttribute("vacantes", serviceVacantes.buscarByExample(example));
		return "home";
	}
	
	/* InitBinder para Strings vacios en el DataBinding. Strings vacios seran seteados a null
	 * Este metodo es necesario para la busqueda con Query By Example. 
	 * */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		//Modificador StringTrimmerEditor con valor TRUE setea los vacios a NULL.
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		
	}
		
	//OBJETOS DISPONIBLES PARA TODAS LOS METODOS DE ESTE CONTROLADOR.
	@ModelAttribute
	public void setGenericos(Model model) {
		Vacante vacanteSearch = new Vacante();
		vacanteSearch.reset();
		model.addAttribute("vacantes", serviceVacantes.buscarDestacadas());
		model.addAttribute("categorias", serviceCategorias.buscarTodas());
		model.addAttribute("search", vacanteSearch);
	}
	
	/* Mapeo del boton Ingresar del Menu. La URL /index esta protegida y se mostrara el login.
	 * Luego de ingresar, se redirecciona a la Url raiz /.
	 * Obtenemos datos del usuario y lo guardamos en sesion.
	 * */
	@GetMapping("/index")
	public String mostrarIndex(Authentication auth, HttpSession session) {
		String username = auth.getName();
		//model.addAttribute("username", auth.getName());
		//System.out.println("user: " + username);
		
		//Mostramos los roles del usaurio
		/*for(GrantedAuthority rol : auth.getAuthorities()) {
			System.out.println("rol: " + rol.getAuthority());
		}*/
		
		if(session.getAttribute("usuario") == null) {
			Usuario usuario = serviceUsuarios.buscarPorUsername(username);
			usuario.setPassword(null); //Para no guardar password en session
			session.setAttribute("usuario", usuario); //almacenamos datos de usuario en session.
			System.out.println("usuario: " + usuario);
		}		

		return "redirect:/";
	}
	
	//Utileria para encriptar con Bcrypt. Solo es usado para cambiar las claves en la bd.
	@GetMapping("/bcrypt/{texto}")
	@ResponseBody //Para no renderizar una vista sino que el resultado se renderize en el navegador.
	public String encriptar(@PathVariable("texto") String texto) {
		return texto + " encriptado es: " + passwordEncoder.encode(texto);
	}
	
	//Llamada a formulario Login personalizado. Con el login por defecto de Spring no seria necesario este metodo.
	@GetMapping("/login")
	public String mostrarLogin() {
		return "formLogin";
	}
	
	//Cierre de sesion. (usado con nuestro login personalizado)
	//Con el login por defecto de Spring, no seria necesario este metodo.
	@GetMapping("/logout")
	public String logout(HttpServletRequest request) {
		
		SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
		logoutHandler.logout(request, null, null); //Cerramos sesion.
		
		//redireccionamos al formulario Login. (metodo anterior a este)
		return "redirect:/login";
	}
}

/* THYMELEAF - EXPRESIONES COMUNES
 * 
 * RENDERIZAR ATRIBUTOS ALMACENADOS EN LA SESION.
 * th:text = "${session.nombreAtributo}"
 * 
 * RENDERIZAR EN LA VISTA EL NOMBRE DEL USUARIO LOGUEADO
 * sec:authentication = "name"
 * 
 * RENDERIZAR UN ELEMENTO HTML PARA USUARIOS ANONIMOS (USUARIOS NO LOGUEADOS)
 * sec:authorize = "isAnonymous()"
 * 
 * RENDERIZAR UN ELEMENTO HTML PARA USUARIOS AUTENTICADOS O LOGUEADOS
 * sec:authorize = "isAuthenticated()"
 * 
 * */
