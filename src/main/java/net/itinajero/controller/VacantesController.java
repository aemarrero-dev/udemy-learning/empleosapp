package net.itinajero.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.itinajero.model.Vacante;
import net.itinajero.service.ICategoriasService;
import net.itinajero.service.IVacantesService;
import net.itinajero.util.Utileria;

@Controller
@RequestMapping(value = "/vacantes")
public class VacantesController {
	
	@Autowired
	private IVacantesService serviceVacantes;
	
	@Autowired
	//@Qualifier("categoriasServiceJPA") //Para indicar cual implementacion usar. La estatica o con Jpa.
	private ICategoriasService serviceCategorias;
	
	@Value("${empleosapp.ruta.imagenes}")
	private String rutaImagenes;
	
	@GetMapping("/index")
	public ModelAndView mostrarIndex() {
		List<Vacante> lista = serviceVacantes.buscarTodas();
		ModelAndView model = new ModelAndView();
		model.addObject("vacantes", lista);
		model.setViewName("vacantes/listVacantes");
		return model;
	}
	
	@GetMapping("/indexPaginate")
	public String mostrarIndexPaginado(Model model, Pageable page) {
		Page<Vacante> lista = serviceVacantes.buscarTodas(page);
		model.addAttribute("vacantes", lista);
		return "vacantes/listVacantes";
	}
	
	//Es necesario enviar objeto Vacante al formulario para vincular errores BindingResult en la vista y mostrarlos.
	@GetMapping("/create")
	public ModelAndView crear(Vacante vacante) {
		ModelAndView model = new ModelAndView();
		model.addObject("categorias", serviceCategorias.buscarTodas());
		model.setViewName("vacantes/formVacante");
		return model;
	}
	
	@GetMapping("/view/{id}")
	public ModelAndView verDetalle(@PathVariable("id") int idVacante) {
		ModelAndView model = new ModelAndView();
		Vacante vacante = serviceVacantes.buscarPorId(idVacante);
		System.out.println("Vacante : " + vacante);
		model.addObject("vacante", vacante);
		model.setViewName("detalle");
		return model;
	}
	
	/*@GetMapping("/delete")
	public String eliminar(@RequestParam("id") int idVacante, Model model) {
		System.out.println("Vacante a eliminar: " + idVacante);
		serviceVacantes.eliminar(idVacante);
		return "redirect:/vacantes/index";
	}*/
	
	//En un escenario real NO SE ELIMINAN LOS REGISTROS SINO QUE SE LE CAMBIA SU ESTATUS.
	@GetMapping("/delete")
	public String eliminar(@RequestParam("id") int idVacante, Model model, RedirectAttributes attributes) {
		System.out.println("Vacante a eliminar: " + idVacante);
		Vacante vacante = serviceVacantes.buscarPorId(idVacante);
		vacante.setEstatus("Eliminada");
		vacante.setDestacado(0);
		serviceVacantes.guardar(vacante);
		

		//Mensaje de confirmacion agregado como FlashAttribute para mostralo en el redirect a listVacantes
		attributes.addFlashAttribute("msg", "Registro Eliminado / Actualizado.");
		
		return "redirect:/vacantes/index";
	}
	
	/* Obtenemos ID desde vista, consultamos la vacante por ID y la enviamos al formulario formVacante. */
	@GetMapping("/edit/{id}")
	public String editar(@PathVariable("id") int idVacante, Model model) {
		Vacante vacante = serviceVacantes.buscarPorId(idVacante);
		model.addAttribute("vacante", vacante);
		model.addAttribute("categorias", serviceCategorias.buscarTodas());
		return "vacantes/formVacante";
	}
	
	//Agregamos BindingResult INMEDIATAMENTE DESPUES de clase Modelo para capturar posibles errores al efectuar DataBinding
	@PostMapping("/save")
	public String guardar(Vacante vacante, BindingResult result, RedirectAttributes attributes, 
			@RequestParam("archivoImagen") MultipartFile multipart) { //DataBinding
		
		//Para mostrar errores de DataBinding en consola.
		if(result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				System.out.println("Error: " + error.getDefaultMessage());
			}
			return "vacantes/formVacante";
		}
		
		//Gestion de la imagen a subir
		if(!multipart.isEmpty()) {
			//String ruta = "/empleos/img-vacantes"; //Linux, Mac
			//String ruta = "d:/empleos/img-vacantes/"; //Windows
			String nombreImagen = Utileria.guardarImagen(multipart, rutaImagenes);
			if(nombreImagen != null) {
				vacante.setImagen(nombreImagen);
			}
		}
		
		serviceVacantes.guardar(vacante);
		System.out.println(vacante.toString());
		
		//Mensaje de confirmacion agregado como FlashAttribute para mostralo en el redirect a listVacantes
		attributes.addFlashAttribute("msg", "Registro Guardado");
		
		return "redirect:/vacantes/index";
	}
	
	@GetMapping("/view2")
	public ModelAndView verDetalle2(@RequestParam("id") int idVacante) {
		ModelAndView model = new ModelAndView();
		System.out.println("RequestParam: IdVacante : " + idVacante);
		model.addObject("idVacante", idVacante);
		model.setViewName("/vacantes/detalle");
		return model;
	}
	
	
	//Establece formato a la fecha leida desde el formulario.
	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, false));
	}
	
	/*//Si habilitamos este codigo, podemos eliminar la insercion de categorias en los metodos crear/editar
	@ModelAttribute
	private void setGenericos(Model model) {
		model.addAttribute("categorias", serviceCategorias.buscarTodas());
		model.addAttribute("vacantes", serviceVacantes.buscarTodas());
	} */
}


/* FLASH ATTRIBUTES - Usados para redirecciones patron POST/Redirect/GET
 * Cuando agregamos atributos al modelo, este atributo estara disponible unicamente para esa peticion (ej. /save).
 * Como estamos aplicando un redirect hacia vacantes/index, implicitamente estamos ejecutando otra peticion.
 * El atributo no estaria disponible para el redirect. Esta disponible solo para la primera peticion u original.
 * En el metodo guardar no usamos ModelAndView porque los atributos van en un solo sentido y los flash attributes
 * van y vienen. 
 * */
