package net.itinajero.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.itinajero.model.Categoria;
import net.itinajero.service.ICategoriasService;

@Controller
@RequestMapping(value="/categorias")
public class CategoriasController {
	
	@Autowired
	@Qualifier("categoriasServiceJPA") //Para indicar cual implementacion usar. La estatica o con Jpa.
	private ICategoriasService serviceCategoria;
	
	@GetMapping("/index")
	public ModelAndView mostrarIndex() {
		ModelAndView model = new ModelAndView();
		model.addObject("categorias", serviceCategoria.buscarTodas());
		model.setViewName("categorias/listCategorias");
		return model;
	}
	
	@GetMapping("/indexPaginate")
	public String mostrarIndexPaginado(Model model, Pageable page) {
		Page<Categoria> lista = serviceCategoria.buscarTodas(page);
		model.addAttribute("categorias", lista);
		return "categorias/listCategorias";
	}
	
	//Es necesario enviar objeto Vacante al formulario para vincular errores BindingResult en la vista.
	@GetMapping("/create")
	public ModelAndView crear(Categoria categoria) {
		ModelAndView model = new ModelAndView();
		model.setViewName("categorias/formCategoria");
		return model;
	}
	
	/* Obtenemos ID desde vista, consultamos la categoria por ID y la enviamos al formulario formCategoria. */
	@GetMapping("/edit/{id}")
	public String editar(@PathVariable("id") int idCategoria, Model model) {
		Categoria vacante = serviceCategoria.buscarPorId(idCategoria);
		model.addAttribute("categoria", vacante);
		return "categorias/formCategoria";
	}
	
	@GetMapping("/delete")
	public String eliminar(@RequestParam("id") int idCategoria, Model model, RedirectAttributes attributes) {
		System.out.println("Vacante a eliminar: " + idCategoria);
		serviceCategoria.eliminar(idCategoria);
		return "redirect:/categorias/index";
	}
	
	@PostMapping("/save")
	public String guardar(Categoria categoria, BindingResult result, RedirectAttributes attributes) {
		
		//Para capturar errores de DataBinding.
		if(result.hasErrors()) {
			for (ObjectError error : result.getAllErrors()) {
				System.out.println("Error: " + error.getDefaultMessage());
			}
			return "categorias/formCategoria";
		}
		serviceCategoria.guardar(categoria);
		
		//Mensaje de confirmacion agregado como FlashAttribute para mostralo en el redirect a listCategorias
		attributes.addFlashAttribute("msg", "Registro Guardado");
	
		return "redirect:/categorias/index";
	}
}

/* FLASH ATTRIBUTES - Usados para redirecciones patron POST/Redirect/GET
 * Cuando agregamos atributos al modelo, este atributo estara disponible unicamente para esa peticion (ej. /save).
 * Como estamos aplicando un redirect hacia vacantes/index, implicitamente estamos ejecutando otra peticion.
 * El atributo no estaria disponible para el redirect. Esta disponible solo para la primera peticion u original.
 * */