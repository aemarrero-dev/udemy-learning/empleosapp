package net.itinajero.util;

import java.io.File;
import java.io.IOException;
import org.springframework.web.multipart.MultipartFile;

public class Utileria {
	
	public static String guardarImagen(MultipartFile multiPart, String ruta) {
		
		//Obtenemos el nombre original del archivo
		String nombreOriginal = multiPart.getOriginalFilename();
		
		//Reemplazamos espacios en blanco por guiones en el nombre del archivo.
		nombreOriginal = nombreOriginal.replace(" ", "-");
		String nombreFinal = randomAlphaNumeric(8) + "_" + nombreOriginal;
		
		
		System.out.println(multiPart.getContentType());
		if("image/png".equalsIgnoreCase(multiPart.getContentType()) || 
		   "image/jpeg".equalsIgnoreCase(multiPart.getContentType())) {
			
			try {
				// Formamos el nombre del archivo para guardarlo en el disco duro
				File imageFile = new File(ruta + nombreFinal);
				System.out.println("file: " + imageFile.getAbsolutePath());
				
				// Aqui se guarda fisicamente el archivo en el disco duro
				multiPart.transferTo(imageFile);
			} catch (IOException e) {
				System.out.println("Error " + e.getMessage());
				nombreFinal = null;
			}
		} else {
			nombreFinal = "noimage.png";
		}
		return nombreFinal;
	}
	
	//Metodo para generar una cadena de longitud N de caracteres aleatorios.
	public static String randomAlphaNumeric(int count) {
		String CARACTERES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * CARACTERES.length());
			builder.append(CARACTERES.charAt(character));
		}
		return builder.toString();
	}
	
	public static String guardarArchivo(MultipartFile multiPart, String ruta) {
		// Obtenemos el nombre original del archivo.
		String nombreOriginal = multiPart.getOriginalFilename();
		// Reemplazamos en el nombre de archivo los espacios por guiones.
		nombreOriginal = nombreOriginal.replace(" ", "-");
		// Agregamos al nombre del archivo 8 caracteres aleatorios para evitar duplicados.
		String nombreFinal = randomAlphaNumeric(8)+nombreOriginal;
		try {
			// Formamos el nombre del archivo para guardarlo en el disco duro.
			File imageFile = new File(ruta + nombreFinal);
			System.out.println("Archivo: " + imageFile.getAbsolutePath());
			//Guardamos fisicamente el archivo en HD.
			multiPart.transferTo(imageFile);
			return nombreFinal;
		} catch (IOException e) {
			System.out.println("Error " + e.getMessage());
			return null;
		}
	}
}
