package net.itinajero.service;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.itinajero.model.Vacante;

public interface IVacantesService {

	List<Vacante> buscarTodas();
	Page<Vacante> buscarTodas(Pageable page);
	Vacante buscarPorId(Integer idVacante);
	List<Vacante> buscarDestacadas();
	void guardar(Vacante vacante);
	void eliminar(Integer idVacante);
	
	
	/* HAREMOS UNA CONSULTA DE TIPO QUERY BY EXAMPLE.
	 * ESTA CONSULTA ES SIMILAR A UNA CONSULTA TRADICIONAL SELECT/WHERE.
	 * LOS VALORES DEL WHERE SERAN LAS PROPIEDADES DEL OBJETO VACANTE QUE NO SEAN NULOS.
	 */
	List<Vacante> buscarByExample(Example<Vacante> example);
}
