package net.itinajero.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.itinajero.model.Vacante;

//Podemos eliminar @Service para corregir conflicto con CategoriasServiceJpa. 
//Usamos @Primary en CategoriasServiceJpa y solucionamos el conflicto igual.
//Podemos usar @Qualifier en CategoriasServiceJpa para indicar que debe ser consumido.
@Service
public class VacantesServiceImpl implements IVacantesService {

	private List<Vacante> lista = null;
	
	public VacantesServiceImpl() {
		super();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		lista = new LinkedList<Vacante>();
		
		try {
			Vacante v1 = new Vacante();
			v1.setId(1);
			v1.setNombre("Full Stack Java");
			v1.setDescripcion("Se solicita Desarrollador Java");
			v1.setFecha(sdf.parse("15-12-2005"));
			v1.setSalario(2000000.00);
			v1.setDestacado(1);
			v1.setEstatus("Creada");
			v1.setImagen("java.png");
			//v1.setCategoria(new Categoria(1, "CCC1", "CCC33"));
			
			Vacante v2 = new Vacante();
			v2.setId(2);
			v2.setNombre("Javascript Developer");
			v2.setDescripcion("Se solicita programador Javascript");
			v2.setFecha(sdf.parse("02-03-2006"));
			v2.setSalario(2100000.00);
			v2.setDestacado(0);
			v2.setEstatus("Aprobada");
			v2.setImagen("js.png");
			
			Vacante v3 = new Vacante();
			v3.setId(3);
			v3.setNombre("Python Developer");
			v3.setDescripcion("Se solicita programador Python");
			v3.setFecha(sdf.parse("02-6-2008"));
			v3.setSalario(2300000.00);
			v3.setDestacado(1);
			v3.setEstatus("Aprobada");
			v3.setImagen("python.png");
			
			Vacante v4 = new Vacante();
			v4.setId(4);
			v4.setNombre("Go Developer");
			v4.setDescripcion("Se solicita programador Go");
			v4.setFecha(sdf.parse("02-06-2008"));
			v4.setSalario(2600000.00);
			v4.setDestacado(0);
			v4.setEstatus("Eliminada");
			v4.setImagen("go.png");
			
			Vacante v5 = new Vacante();
			v5.setId(5);
			v5.setNombre("Big Data Analista");
			v5.setDescripcion("Se solicita Analista de Datos");
			v5.setFecha(sdf.parse("02-10-2008"));
			v5.setSalario(2500000.00);
			v5.setDestacado(1);
			v5.setEstatus("Creada");
			v5.setImagen("bigdata.png");
			
			lista.add(v1);
			lista.add(v2);
			lista.add(v3);
			lista.add(v4);
			lista.add(v5);
			
		} catch (ParseException e) {
			System.out.println("Error: " + e.getMessage());
		}	
	}

	@Override
	public List<Vacante> buscarTodas() {
		return lista;
	}

	@Override
	public Vacante buscarPorId(Integer idVacante) {
		for (Vacante vacante : lista) {
			if(vacante.getId() == idVacante) {
				return vacante;
			}
		}
		return null;
	}

	@Override
	public void guardar(Vacante vacante) {
		lista.add(vacante);
		
	}

	@Override
	public List<Vacante> buscarDestacadas() {
		return null;
	}

	@Override
	public void eliminar(Integer idVacante) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Vacante> buscarByExample(Example<Vacante> example) {
		return null;
	}

	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

}
