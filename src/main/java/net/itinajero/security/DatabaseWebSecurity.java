package net.itinajero.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class DatabaseWebSecurity extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource datasource;
	
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		//Con esta linea usamos la configuracion basica por defecto, usuario/clave de las tablas de Spring Security (users y authorities).
		auth.jdbcAuthentication().dataSource(datasource)
		
		/* Con estas sentencias estamos utlizando el usuario/password de nuestras tablas personalizadas (usuarios, perfiles y usuarioPerfil).
		 * Dejamos de usar las tablas por defecto de Spring Security. (users y authorities).
		 * Los querys deben devolver username, password y estatus en el mismo orden de la tabla de Spring users y recibir de entrada el username.
		 * 
		 * */
		.usersByUsernameQuery("select username, password, estatus from Usuarios where username = ?")
		.authoritiesByUsernameQuery("select u.username, p.perfil from UsuarioPerfil up "
				+ "inner join Usuarios u on u.id = up.idUsuario "
				+ "inner join Perfiles p on p.id = up.idPerfil "
				+ "where u.username = ?");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		/* EN ESTE METODO CONFIGURAMOS LAS URLs QUE ESTARAN PROTEGIDAS AL PUBLICO.
		 * 1. Recursos estaticos no requieren autenticacion.
		 * 2. La vistas publicas no requieren autenticacion.
		 * 3. Se define acceso por roles.
		 * 3. Todas lsa demas URLs de la aplicacion requieren autenticacion.
		 * 4. Formulario de login no requiere autenticacion.
		 * */
		http.authorizeRequests()
		.antMatchers("/bootstrap/**", "/images/**", "/tinymce/**", "logos/**").permitAll() // estaticos
		.antMatchers("/", "/signup", "/search", "/vacantes/view/**", "/bcrypt/**").permitAll() //vistas publicas

		.antMatchers("/vacantes/**").hasAnyAuthority("SUPERVISOR, ADMINISTRADOR") //acceso por roles
		.antMatchers("/categorias/**").hasAnyAuthority("SUPERVISOR, ADMINISTRADOR")
		.antMatchers("/usuarios/**").hasAnyAuthority("ADMINISTRADOR")
		
		.anyRequest().authenticated() //el resto de URLs requieren autenticacion
		
		//Login de Spring Security por defecto - NO requiere autenticacion, es visible.
		//.and().formLogin().permitAll();
		
		//Usamos nuestra ventana login personalizada. Con esta linea dejamos de usar el form login de Spring Security
		//Implica que debemos gestionar nosotros el cierre de sesion. logout.
		.and().formLogin().loginPage("/login").permitAll(); //ventana login NO requiere autenticacion
	}
	
	/* CON SOLO AGREGAR ESTA DEFINICION, SPRING ASUMIRA QUE TODOS LOS PASSWORDS DE LA BD ESTAN ENCRIPTADAS.
	 * POR ESA RAZON NO SE PUEDE ENTRAR CON ALGUNO DE LOS USUARIOS YA REGISTRADOS.
	 * SE DEBE ENCRIPTAR SUS PASSWORD Y ACTUALIZARLOS EN LA BD PARA PODER INGRESAR NUEVAMENT.
	 * */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}

/* Con esta clase inyectamos el datasource definido en application.properties.
 * Dejamos de usar el usuario y password estaticos definidos en application.properties.
 * Ahora el login sera con el usuario y clave de la base de datos (tablas user y authorities) de Spring. (linea 21)
 *
 * */