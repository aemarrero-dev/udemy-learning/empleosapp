package net.itinajero.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {
	
	@Value("${empleosapp.ruta.imagenes}")
	private String rutaImagenes; 
	
	@Value("${empleosapp.ruta.cv}")
	private String rutaCv;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//registry.addResourceHandler("/logos/**").addResourceLocations("file:/empleos/img-vacantes/"); //linux
		//registry.addResourceHandler("/imagenes/**").addResourceLocations("file:d/empleos/img-vacantes/"); //windows
		
		//registry.addResourceHandler("/logos/**").addResourceLocations("file:"+rutaImagenes); //windows
		
		// Configuración de los recursos estáticos (imagenes de las vacantes) 
		//registry.addResourceHandler("/logos/**").addResourceLocations("file:c:/empleos/img-vacantes/"); // Windows
		registry.addResourceHandler("/imagenes/**").addResourceLocations("file:/empleos/img-vacantes/"); // Linux
		//registry.addResourceHandler("/logos/**").addResourceLocations("file:"+rutaImagenes); 
		
		// Configuración de los recursos estáticos (archivos de los CV)
		//registry.addResourceHandler("/cv/**").addResourceLocations("file:d:/empleos/files-cv/"); // Windows
		//registry.addResourceHandler("/cv/**").addResourceLocations("file:/empleos/files-cv/"); // Linux
		registry.addResourceHandler("/cv/**").addResourceLocations("file:"+rutaCv); 
	}
}

/* Ahora las imagenes se accederan 
 * Configuramos una ruta adicional que contendra las imagenes subidas de las vacantes.
 * Por defecto en SpringBoot y Thymeleaf las imagenes se guardan en src/main/resources/static.
 * Si necesitamos mapear recursos de otras ubicaciones seria de esa manera arriba.
 */