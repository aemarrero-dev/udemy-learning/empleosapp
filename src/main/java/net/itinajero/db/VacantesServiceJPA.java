package net.itinajero.db;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.itinajero.model.Vacante;
import net.itinajero.repository.IVacantesRepository;
import net.itinajero.service.IVacantesService;

@Service
@Primary
public class VacantesServiceJPA implements IVacantesService {

	@Autowired
	private IVacantesRepository repo;
	
	@Override
	public List<Vacante> buscarTodas() {
		return repo.findAll();
	}
	
	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Vacante buscarPorId(Integer idVacante) {
		Optional<Vacante> op = repo.findById(idVacante);
		if(op.isPresent()) {
			return op.get();
		}
		return null;
	}

	@Override
	public void guardar(Vacante vacante) {
		repo.save(vacante);
		
	}

	@Override
	public List<Vacante> buscarDestacadas() {
		return repo.findByDestacadoAndEstatusOrderByIdDesc(1, "Aprobada");
	}

	@Override
	public void eliminar(Integer idVacante) {
		repo.deleteById(idVacante);
		
	}

	// Query By Example, realiza la busqueda de acuerdo a las propiedades NO NULAS del objeto Vacante.
	@Override
	public List<Vacante> buscarByExample(Example<Vacante> example) {
		return repo.findAll(example);
	}

}

//Podemos eliminar @Service para solucionar conflicto de 2 implementaciones de IVacantesService. 
//Usamos @Primary en VacantesServiceJpa y solucionamos el conflicto igual.
//Podemos usar @Qualifier en donde inyectamos IVacantesService y especificar cual implementacion usar (jpa o estatica).

