package net.itinajero.db;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.itinajero.model.Categoria;
import net.itinajero.repository.ICategoriasRepository;
import net.itinajero.service.ICategoriasService;

@Service
@Primary
public class CategoriasServiceJPA implements ICategoriasService {

	@Autowired
	private ICategoriasRepository repo;
	
	@Override
	public void guardar(Categoria categoria) {
		repo.save(categoria);
	}

	@Override
	public List<Categoria> buscarTodas() {
		return repo.findAll();
	}
	
	@Override
	public Page<Categoria> buscarTodas(Pageable page) { 
		return repo.findAll(page);
	}

	@Override
	public Categoria buscarPorId(Integer idCategoria) {
		Optional<Categoria> op = repo.findById(idCategoria);
		if(op.isPresent()) {
			return op.get();
		}
		return null;
	}

	@Override
	public void eliminar(Integer idCategoria) {
		repo.deleteById(idCategoria);
		
	}

}

//Podemos eliminar @Service para solucionar conflicto de 2 implementaciones de ICategoriasService. 
//Usamos @Primary en CategoriasServiceJpa y solucionamos el conflicto igual.
//Podemos usar @Qualifier en donde inyectamos ICategoriasService y especificar cual implementacion usar (jpa o estatica).
