package net.itinajero.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.itinajero.model.Usuario;
import net.itinajero.repository.IUsuariosRepository;
import net.itinajero.service.IUsuariosService;

@Service
public class UsuariosServiceJPA implements IUsuariosService {

	@Autowired
	private IUsuariosRepository repo;
	
	@Override
	public void guardar(Usuario usuario) {
		repo.save(usuario);
	}

	@Override
	public void eliminar(Integer idUsuario) {
		repo.deleteById(idUsuario);

	}

	@Override
	public List<Usuario> buscarTodos() {
		return repo.findAll();
	}

	@Override
	public Usuario buscarPorUsername(String username) {
		return repo.findByUsername(username);
	}

}
