APLICACION EMPLEOS - CURSO UDEMY.

Esta aplicacion utiliza los conceptos de Spring MVC, Thymeleaf, Spring Data JPA, etc., aprendidos en el curso de Udemy (Ivan Tinajero). La aplicación se conecta a una base de datos MySQL alojado en un servidor en la nube.

ACCESO A LA APP
http://45.79.84.26:8081/empleosapp/

CREDENCIALES DE PRUEBA

usuario: luis
clave: luis123

usuario: marisol
clave: mari123